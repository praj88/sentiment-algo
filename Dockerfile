FROM ujwal19/ample-tensorflow:latest

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# Copy the application folder inside the container
ADD /app-sentiment-algo /app-sentiment-algo


# Get pip to download and install requirements:
RUN pip install -r /app-sentiment-algo/requirements.txt

# install h5py for loaded DL model in keras and pandas
RUN pip install h5py


# Expose ports
EXPOSE 5005

# Set the default directory where CMD will execute
WORKDIR /app-sentiment-algo
CMD python /app-sentiment-algo/sentiment_DL.py
