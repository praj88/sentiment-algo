#from pycorenlp import StanfordCoreNLP
#import os
#os.chdir('/app-sentiment-algo')

import pandas as pd
import numpy as np
import keras

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.layers import Bidirectional
from keras.preprocessing import sequence
from keras.layers import Dropout
import h5py
import utility_functions as uf
from keras.models import model_from_json
from keras.models import load_model
from flask import Flask, url_for, request
import json
from nltk.tokenize import RegexpTokenizer



weight_path = '/app-sentiment-algo/best_weight_glove_bi_100d.hdf5'
#weight_path_2 = '/Users/prajwalshreyas/Desktop/Singularity/dockerApps/DL_sentiment/app-dl-sentiment-v3/checkpoint/Bi-RNN-Glove/v2/archive/best_weight_glove_bi_100d.hdf5'
prd_model = load_model(weight_path)
prd_model.summary()
#word_idx = uf.load_word_idx(gloveFile)
word_idx = json.load(open("/app-sentiment-algo/word_idx.txt"))
#type(word_idx_json)

app = Flask(__name__)
@app.route('/sentest')
def sentest():
    #print ('enter sentest')
    #count = redis.incr('hits')
    return 'Sentiment API working'

# main sentiment code
@app.route('/sentiment', methods=['POST'])
def sentiment():

    if request.method == 'POST':
        text_data = pd.DataFrame(request.json)
        #text_data = pd.read_json('/app-sentiment-algo/sample_text.json')

        # Deep Learning
        #text_out = get_sentiment_DL(prd_model, text_data, word_idx)
        text_out = get_sentiment_DL(prd_model, text_data, word_idx)
        #text_out.to_csv('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/sentiment-algo/app-sentiment-algo/sample_text_DL_100.csv')

        text_out = text_out[['ref','Sentiment_Score']]

        #Convert df t dict and the to Json
        text_out_dict = text_out.to_dict(orient='records')
        text_out_json = json.dumps(text_out_dict, ensure_ascii=False)

        return text_out_json

def get_sentiment_DL(prd_model, text_data, word_idx):

    #data = "Pass the salt"

    live_list = []
    batchSize = len(text_data)
    live_list_np = np.zeros((56,batchSize))
    for index, row in text_data.iterrows():
        #print (index)
        text_data_sample = text_data['text'][index]

        # split the sentence into its words and remove any punctuations.
        tokenizer = RegexpTokenizer(r'\w+')
        text_data_list = tokenizer.tokenize(text_data_sample)

        #text_data_list = text_data_sample.split()


        labels = np.array(['1','2','3','4','5','6','7','8','9','10'], dtype = "int")
        #word_idx['I']
        # get index for the live stage
        data_index = np.array([word_idx[word.lower()] if word.lower() in word_idx else 0 for word in text_data_list])
        data_index_np = np.array(data_index)

        # padded with zeros of length 56 i.e maximum length
        padded_array = np.zeros(56)
        padded_array[:data_index_np.shape[0]] = data_index_np
        data_index_np_pad = padded_array.astype(int)


        live_list.append(data_index_np_pad)

    live_list_np = np.asarray(live_list)
    score = prd_model.predict(live_list_np, batch_size=batchSize, verbose=0)
    single_score = np.round(np.dot(score, labels)/10,decimals=2)

    score_all  = []
    for each_score in score:

        top_3_index = np.argsort(each_score)[-3:]
        top_3_scores = each_score[top_3_index]
        top_3_weights = top_3_scores/np.sum(top_3_scores)
        single_score_dot = np.round(np.dot(top_3_index, top_3_weights)/10, decimals = 2)
        score_all.append(single_score_dot)

    text_data['Sentiment_Score'] = pd.DataFrame(score_all)

    return text_data

def get_sentiment(text_data):
    #uniqueTweets = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/Twitter/sample_tweets.csv')
    #uniqueTweets = uniqueTweets[0:500]
    text_data['Sentiment_compunded'] = ""
    text_data['Sentiment_pos'] = ""
    text_data['Sentiment_neg'] = ""
    text_data['Sentiment_neu'] = ""


    #uniqueTweets_text = uniqueTweets['text']

    sid = SentimentIntensityAnalyzer()
    for index, row in text_data.iterrows():
        #print (index)
        tweets = text_data['text'][index]
        #print(tweets)
        ss = sid.polarity_scores(tweets)
        text_data.loc[index,'Sentiment_compunded']= ss['compound']
        text_data.loc[index,'Sentiment_pos'] = ss['pos']
        text_data.loc[index,'Sentiment_neg'] = ss['neg']
        text_data.loc[index,'Sentiment_neu'] = ss['neu']


    text_data_Sentiment = text_data.copy()
    text_data_Sentiment['Sentiment_Type'] = np.where(text_data_Sentiment['Sentiment_compunded']>0, 'Positive',np.where(text_data_Sentiment['Sentiment_compunded']<0, 'Negative',np.where(text_data_Sentiment['Sentiment_compunded']==0, 'Neutral','nan')))
    text_data_Sentiment['Sentiment_Score'] = np.where(text_data_Sentiment['Sentiment_compunded'].abs() <= 0.33, 1 ,np.where(text_data_Sentiment['Sentiment_compunded'].abs() <=0.66, 2, np.where(text_data_Sentiment['Sentiment_compunded'].abs()>0.66, 3, 'nan')))
    text_data_Sentiment['Sentiment_Score'] = np.where(text_data_Sentiment['Sentiment_Type']== "Neutral", 0, text_data_Sentiment['Sentiment_Score'])

    del text_data_Sentiment['Sentiment_compunded']
    del text_data_Sentiment['Sentiment_pos']
    del text_data_Sentiment['Sentiment_neg']
    del text_data_Sentiment['Sentiment_neu']


    return  text_data_Sentiment

if __name__ == "__main__":
     app.run(host="0.0.0.0", debug=False, port=5005)
