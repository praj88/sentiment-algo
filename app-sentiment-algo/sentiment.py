#from pycorenlp import StanfordCoreNLP
import pandas as pd
# import subprocess
# from subprocess import Popen, PIPE
# import sh
# import threading
# from threading import Thread
#from string import punctuation
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import numpy as np
from flask import Flask, url_for, request
import json
DEBUG = False

app = Flask(__name__)
@app.route('/sentest')
def sentest():
    #print ('enter sentest')
    #count = redis.incr('hits')
    return 'Sentiment API working'
    
# main sentiment code
@app.route('/sentiment', methods=['POST'])
def sentiment():

    if request.method == 'POST':
        text_data = pd.DataFrame(request.json)
        #text_data = pd.read_json('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/sentiment-algo/app-sentiment-algo/sample_text.json')
        text_out = get_sentiment(text_data)
        text_out = text_out[['ref','Sentiment_Type','Sentiment_Score']]

        #Convert df t dict and the to Json
        text_out_dict = text_out.to_dict(orient='records')
        text_out_json = json.dumps(text_out_dict, ensure_ascii=False)
        return text_out_json



def get_sentiment(text_data):
    #uniqueTweets = pd.read_csv('/Users/prajwalshreyas/Desktop/Singularity/Twitter/sample_tweets.csv')
    #uniqueTweets = uniqueTweets[0:500]
    text_data['Sentiment_compunded'] = ""
    text_data['Sentiment_pos'] = ""
    text_data['Sentiment_neg'] = ""
    text_data['Sentiment_neu'] = ""


    #uniqueTweets_text = uniqueTweets['text']

    sid = SentimentIntensityAnalyzer()
    for index, row in text_data.iterrows():
        #print (index)
        tweets = text_data['text'][index]
        #print(tweets)
        ss = sid.polarity_scores(tweets)
        text_data.loc[index,'Sentiment_compunded']= ss['compound']
        text_data.loc[index,'Sentiment_pos'] = ss['pos']
        text_data.loc[index,'Sentiment_neg'] = ss['neg']
        text_data.loc[index,'Sentiment_neu'] = ss['neu']


    text_data_Sentiment = text_data.copy()
    text_data_Sentiment['Sentiment_Type'] = np.where(text_data_Sentiment['Sentiment_compunded']>0, 'Positive',np.where(text_data_Sentiment['Sentiment_compunded']<0, 'Negative',np.where(text_data_Sentiment['Sentiment_compunded']==0, 'Neutral','nan')))
    text_data_Sentiment['Sentiment_Score'] = np.where(text_data_Sentiment['Sentiment_compunded'].abs() <= 0.33, 1 ,np.where(text_data_Sentiment['Sentiment_compunded'].abs() <=0.66, 2, np.where(text_data_Sentiment['Sentiment_compunded'].abs()>0.66, 3, 'nan')))
    text_data_Sentiment['Sentiment_Score'] = np.where(text_data_Sentiment['Sentiment_Type']== "Neutral", 0, text_data_Sentiment['Sentiment_Score'])

    del text_data_Sentiment['Sentiment_compunded']
    del text_data_Sentiment['Sentiment_pos']
    del text_data_Sentiment['Sentiment_neg']
    del text_data_Sentiment['Sentiment_neu']


    return  text_data_Sentiment

if __name__ == "__main__":
     app.run(host="0.0.0.0", debug=False, port=5005)
